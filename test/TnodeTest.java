
import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author Jaanus
 */
public class TnodeTest {

   @Test (timeout=1000)
   public void testBuildFromRPN() { 
      String s = "1 2 +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(1,2)", r);
      s = "2 1 - 4 * 6 3 / +";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(6,3))", r);
   }

   @Test (timeout=1000)
   public void testBuild2() {
      String s = "512 1 - 4 * -61 3 / +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString();
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-61,3))", r);
      s = "5";
      t = Tnode.buildFromRPN (s);
      r = t.toString();
      assertEquals ("Tree: " + s, "5", r);
   }

   @Test (timeout=1000)
   public void testBuildExtended() {
      String a = "2 5 SWAP -";
      Tnode at = Tnode.buildFromRPN (a);
      String as = at.toString();
      assertEquals ("Tree: " + a, "-(5,2)", as);

      String b = "3 DUP *";
      Tnode bt = Tnode.buildFromRPN (b);
      String bs = bt.toString();
      assertEquals ("Tree: " + b, "*(3,3)", bs);

      String c = "2 5 9 ROT - +";
      Tnode ct = Tnode.buildFromRPN (c);
      String cs = ct.toString();
      assertEquals ("Tree: " + c, "+(5,-(9,2))", cs);

      String d = "2 5 9 ROT + SWAP -";
      Tnode dt = Tnode.buildFromRPN (d);
      String ds = dt.toString();
      assertEquals ("Tree: " + d, "-(+(9,2),5)", ds);

      String e = "2 5 DUP ROT - + DUP *";
      Tnode et = Tnode.buildFromRPN (e);
      String es = et.toString();
      assertEquals ("Tree: " + e, "*(+(5,-(5,2)),+(5,-(5,2)))", es);

      String f = "-3 -5 -7 ROT - SWAP DUP * +";
      Tnode ft = Tnode.buildFromRPN (f);
      String fs = ft.toString();
      assertEquals ("Tree: " + f, "+(-(-7,-3),*(-5,-5))", fs);
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol() {
      Tnode t = Tnode.buildFromRPN ("2 xx");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol2() {
      Tnode t = Tnode.buildFromRPN ("x");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol3() {
      Tnode t = Tnode.buildFromRPN ("2 1 + xx");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers() {
      Tnode root = Tnode.buildFromRPN ("2 3");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers2() {
      Tnode root = Tnode.buildFromRPN ("2 3 + 5");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers() {
      Tnode t = Tnode.buildFromRPN ("2 -");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers2() {
      Tnode t = Tnode.buildFromRPN ("2 5 + -");
   }

  @Test (expected=RuntimeException.class)
   public void testTooFewNumbers3() {
      Tnode t = Tnode.buildFromRPN ("+");
   }
}

