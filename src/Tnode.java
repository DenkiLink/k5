import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode(String n) {
      name = n;
   }

   @Override
   public String toString() {
      StringBuilder b = new StringBuilder();
      b.append(name);
      if (hasChild()) {
         b.append("(");
         Tnode child = firstChild;
         b.append(child.toString());
         while (child.hasNext()) {
            b.append(",");
            child = child.nextSibling;
            b.append(child.toString());
         }
         b.append(")");
      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      if (pol == null || pol.equals("") || pol.replaceAll(" ", "").equals("")) {
         throw new RuntimeException(String.format("Expression '%s' is empty!", pol));
      }

      String[] exp = pol.split(" ");

      Stack<Tnode> nodes = new Stack<>();
      for (String c : exp) {
         c = c.toUpperCase();
         Tnode node = new Tnode(c);
         if (!c.equals("+") && !c.equals("-") && !c.equals("*") && !c.equals("/") && !c.equals("DUP")
                 && !c.equals("SWAP") && !c.equals("ROT")) {
            try {
               Integer.parseInt(c);
            } catch (RuntimeException e){
               throw new RuntimeException(String.format("Illegal symbol '%s' in expression '%s'", c, pol));
            }
         } else if (c.equals("DUP") && nodes.size() >= 1) {
            nodes.push(nodes.peek().clone());
         } else if (c.equals("SWAP") && nodes.size() >= 2) {
            Tnode a = nodes.pop();
            Tnode b = nodes.pop();
            nodes.push(a);
            nodes.push(b);
         } else if (c.equals("ROT") && nodes.size() >= 3) {
            nodes.push(nodes.elementAt(0));
            nodes.removeElementAt(0);
         } else if (nodes.size() >= 2) {
            Tnode a = nodes.pop();
            Tnode b= nodes.pop();
            node.addChild(b);
            node.addChild(a);
         } else {
            throw new RuntimeException(String.format("Too few numbers in expression '%s'!", pol));
         }

         if (!c.equals("DUP") && !c.equals("SWAP") && !c.equals("ROT")) {
            nodes.push(node);
         }
      }

      if (nodes.size() > 1) {
         throw new RuntimeException(String.format("Too many numbers in expression '%s'!", pol));
      }

      return nodes.pop();
   }

   public static void main (String[] param) {
      String rpn = "-3 -5 -7 ROT - SWAP DUP * +";
      Tnode res = buildFromRPN (rpn);
      System.out.println ("RPN: " + rpn);
      System.out.println ("Tree: " + res);
   }

   private void addChild (Tnode a) {
      if (a == null) return;
      if (!hasChild())
         firstChild = a;
      else {
         Tnode child = firstChild;
         while (child.hasNext()) {
            child = child.nextSibling;
         }
         child.nextSibling = a;
      }
   }

   public Tnode clone() {
      Tnode copy = new Tnode(name);
      copy.firstChild = firstChild;
      copy.nextSibling = nextSibling;
      return copy;
   }

   private boolean hasChild() {
      return firstChild != null;
   }

   private boolean hasNext() {
      return (nextSibling != null);
   }
}